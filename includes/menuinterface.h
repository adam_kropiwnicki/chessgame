#ifndef INTERFACE_H
#define INTERFACE_H

#include<QFrame>
#include<QVBoxLayout>
#include<QGridLayout>
#include<QPushButton>
#include<QComboBox>
#include<QLabel>
#include"numlineedit.h"
#include"numeditvalidator.h"
#include"configurtion.h"
#include<iostream>

class MenuInterface : public QFrame
{
    Q_OBJECT
    QVBoxLayout *layout;
    QPushButton *hostButton;
    QPushButton *exitButton;
    QPushButton *connectButton;
    QPushButton *backButton;
    QPushButton *startButton;
    QComboBox *whitecontrollerBox;
    QComboBox *blackcontrollerBox;

    QFrame* game_settings;
    QGridLayout * game_settings_sublayout;
    QLabel* rows_edit_label;
    QLabel* cols_edit_label;
    QLabel* tileSize_edit_label;
    QLabel* whitecontroller_label;
    QLabel* blackcontroller_label;
    NumLineEdit* rows_edit;
    NumLineEdit* cols_edit;
    NumLineEdit* tileSize_edit;
    QValidator *boardSizeEditValidator;
    QValidator *tileSizeEditValidator;

    QSize * minButtonSize;
    QSize * maxButtonSize;
public:
    MenuInterface(QWidget*);
    ~MenuInterface();
private slots:
    bool showLobbyMenu();
    bool isValidated();
    bool prepareStartUp();
public slots:
    bool showMainMenu();
signals:
    void gameSet();
    void setWhiteController(int);
    void setBlackController(int);
};

#endif // INTERFACE_H
