#ifndef NUMLINEEDIT_H
#define NUMLINEEDIT_H

#include <QLineEdit>
#include "utility.h"

class NumLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    NumLineEdit(QWidget *parent = Q_NULLPTR);
private slots:
    void sendValue();
signals:
    void valueSent(int);
};

#endif // NUMLINEEDIT_H
