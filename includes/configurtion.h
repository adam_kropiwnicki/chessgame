#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QObject>
#include <QColor>

struct Situation {
    char ** pieces;
    bool startingPlayer;
    int columns,rows;
    Situation(int x,int y);
    Situation(const Situation& copied);
    ~Situation();
    QString toQString() const;
    bool fromQString(QString from);
};


class Configuration : public QObject
{
    Q_OBJECT
    Configuration(QObject *parent);
    virtual ~Configuration();
    int _rows,_cols;
    int _tileSize;
    QColor * _blacksColor,*_whitesColor;
    Situation * startingPoint;
    int _blackController,_whiteController;
public:
    static Configuration& config();
    Configuration(Configuration&) = delete;
    Configuration& operator=(Configuration&)=delete;
    int rows() const { return _rows; }
    int cols() const { return _cols; }
    int tile_size() const { return _tileSize; }
    Situation startPoint() const { return *startingPoint; }
    QColor whitesColor() const { return *_whitesColor; }
    QColor blacksColor() const { return *_blacksColor; }
    int blackController() const { return _blackController; }
    int whiteController() const { return _whiteController; }
    bool isValid() const;
    enum Controller { INVALID = 0, YOU = 1, HUMAN = 2, AI = 3 };
private slots:
    bool set_rows(int);
    bool set_cols(int);
    bool set_tileSize(int);
    bool set_StandardGame();
    bool set_StartingPoint(Situation);
    bool set_whiteController(int);
    bool set_blackController(int);
    void reset();
};
#endif // CONFIGURATION_H
