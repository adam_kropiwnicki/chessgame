#ifndef MOVE_H
#define MOVE_H

#include "piece.h"
#include "tile.h"

struct Move  // 'FROM' MEMBER AND 'TAKEN' MEMBER IS INITIATED IMPLICTLY IN CONSTRUCTOR!
{
    Piece *piece;
    Piece *taken;
    Tile *from;
    Tile *target;
    char  * promotion;
    Move(Piece*,Tile*);
    Move(const Move &);
    Move& operator=(Move);
    ~Move();
};

#endif // MOVE_H
