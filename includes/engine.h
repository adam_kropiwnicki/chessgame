#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QMessageBox>
#include <QPushButton>
#include <QPixmap>
#include <QIcon>
#include "configurtion.h"
#include "board.h"
#include "piece.h"
#include "tile.h"


class Engine : public QObject
{
    Q_OBJECT
    QMessageBox * w_promotionDialog;
    QMessageBox * b_promotionDialog;
    std::vector<QPushButton*> w_promotionButtons;
    std::vector<QPushButton*> b_promotionButtons;

public:
    Engine(QWidget *);
    virtual ~Engine();
    bool checkforcheck(Board *,bool);
    bool checkforcheckmate(Board *,bool);
public slots:
    bool processMove(Board*,Move);
};
#endif // ENGINE_H
