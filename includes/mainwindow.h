#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "configurtion.h"
#include "menuinterface.h"
#include "gameinterface.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
    MenuInterface *menuInterface;
    GameInterface *gameInterface;
    QLabel *winScreen;
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    bool startGame();
    void endGame(int);
    void menu();
signals:
    void resetConfiguration();
};

#endif // MAINWINDOW_H
