#ifndef CLIENT_H
#define CLIENT_H

#include <QWidget>
#include <QtNetwork>
#include <QDialog>
#include <QLineEdit>
#include <QMessageBox>
#include "configurtion.h"
#include "move.h"

class Client : public QWidget
{
    Q_OBJECT
    QTcpSocket * socket;
    QDialog * interfaceDialog;
    QLineEdit * ipEdit;
    QLineEdit * portEdit;
    QMessageBox * errorDialog;
    QDialog * connectingDialog;
    QDataStream in;
public:
    explicit Client(QWidget *parent = nullptr);
    virtual ~Client();
signals:
    void goBack();
    void setConfigurationCols(int);
    void setConfigurationRows(int);
    void setConfigurationTile_size(int);
    void setConfigurationStartPoint(Situation);
    void setConfigurationBlackController(int);
    void setConfigurationWhiteController(int);
    void requestMove(int,int,int,int,char);
private slots:
    bool makeConnection();
    void errorHandling(QAbstractSocket::SocketError);
    void decryptMessage();
    void sendMove(Move);
public slots:
    void run();
};

#endif // CLIENT_H
