#ifndef BOARD_H
#define BOARD_H

#include <QGraphicsObject>
#include "tile.h"
#include "piece.h"
#include <vector>
#include <iostream>
#include "configurtion.h"
#include "move.h"

class Board : public QGraphicsObject
{
    Q_OBJECT
    friend class Engine;
    friend class AI;
    QSize _size;
    QRectF _boundingRect;
    Tile *** tiles;
    std::vector<Piece*> whites;
    std::vector<Piece*> blacks;
    std::vector<Move> history;
    Situation * current;
    Piece * _blackKing,*_whiteKing;
    const int _rows,_cols;
    bool _turn;
    bool _ok;
    int id;
    bool _endgame;

    bool initiateTiles();
    bool initiatePieces();
    Piece * blackKing() { return _blackKing; }
    Piece * whiteKing() { return _whiteKing; }
    void move(Move);
    void graphicMove(Move);
    void undoGraphicMove(Move);
    void anchorPiece(Piece*);
    void createPiece(int,bool,Tile *);
    void removePiece(Piece *);
    void resetBoard();
    void restoreBoard(Board *);
    Tile* tileAt(QPointF) const;
public:
    Board();
    virtual ~Board() override;
    QRectF boundingRect() const override;
    void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override;
    Situation situation() const;
    bool turn() const { return _turn; }
private slots:
    void moveRequested(Piece*, QPointF);
    void moveRequested(int,int,int,int,char);
    void undoMove();
signals:
    void callEngine(Board*,Move);
    void callAI(Board*);
    void commitMove(Move);
    void gameOver(int);
};

#endif // BOARD_H
