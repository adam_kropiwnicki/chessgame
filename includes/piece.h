#ifndef PIECE_H
#define PIECE_H

#include <QGraphicsObject>
#include <QPixmap>
#include <QGraphicsSceneMouseEvent>
#include "tile.h"
#include <vector>

class Tile;
class Piece : public QGraphicsObject
{
    Q_OBJECT
    bool _color;
protected:
    QPixmap _image;
    Tile * _tile;
    QRectF _boundingRect;
    int has_moved;
public:
    Piece(QPointF,bool,QGraphicsObject *);
    virtual ~Piece() override;
    enum TYPE{ PAWN = UserType +1, KNIGHT = UserType +2, BISHOP = UserType +3, ROCK = UserType +4, QUEEN = UserType +5, KING = UserType +6 };
    virtual QRectF boundingRect() const override =0;
    virtual void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override =0;
    virtual bool canMove(std::vector<Tile*>*,Tile * target = nullptr) const =0;
    virtual int type() const override =0;
    bool isBlack() const { return _color; }
    Tile * tile() const { return _tile; }
    void setTile(Tile* t) { _tile = t; }
    int hasMoved() const { return has_moved; }
    void justMoved() { has_moved++; }
    void justUndoMoved() { if (has_moved!=0) has_moved--; }
    void setMovedFlag() { has_moved = true; }
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
signals:
    void moving(Piece *,QPointF);
};

#endif // PIECE_H
