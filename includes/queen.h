#ifndef QUEEN_H
#define QUEEN_H


#include "piece.h"

class Queen : public Piece
{
public:
    Queen(QPointF,bool,QGraphicsObject *);
    virtual ~Queen() override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override;
    virtual bool canMove(std::vector<Tile*>*,Tile * target = nullptr) const override;
    virtual int type() const override { return QUEEN; }
};

#endif // QUEEN_H
