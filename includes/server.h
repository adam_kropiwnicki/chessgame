#ifndef SERVER_H
#define SERVER_H

#include <QWidget>
#include <QtNetwork>
#include "move.h"

class Server : public QWidget
{
    Q_OBJECT
    QTcpServer * server;
    QTcpSocket * socket;
    QString _ipAddress;
    int _port;
    QDataStream in;
public:
    explicit Server(QWidget *parent = nullptr);
    virtual ~Server();
signals:
    void requestMove(int,int,int,int,char);
public slots:
    void sendConfiguration();
    void sendMove(Move);
    void decryptMessage();
};

#endif // SERVER_H
