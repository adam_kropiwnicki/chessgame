#ifndef KING_H
#define KING_H

#include "piece.h"

class King : public Piece
{
public:
    King(QPointF,bool,QGraphicsObject *);
    virtual ~King() override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override;
    virtual bool canMove(std::vector<Tile*>*,Tile * target = nullptr) const override;
    virtual int type() const override { return KING; }
};

#endif // KING_H
