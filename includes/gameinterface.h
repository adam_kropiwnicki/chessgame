#ifndef GAMEINTERFACE_H
#define GAMEINTERFACE_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QFrame>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include "board.h"
#include "engine.h"
#include "human.h"
#include "ai.h"
#include "server.h"
#include "client.h"

class GameInterface : public QFrame
{
    Q_OBJECT
    QGraphicsScene * scene;
    QGraphicsView *view;
    QLabel *w_text1,*w_text2,*b_text1,*b_text2;
    QPushButton *undoButton;
    QGridLayout *_layout;
    Board *_board;
    Engine *_engine;
    Player * whitePlayer;
    Player * blackPlayer;
    Server * server;
    Client * client;
    AI * minMax_ai;
    bool state;
    void initBoard();
public:
    GameInterface(QWidget*);
    ~GameInterface();
    bool isValid() const { return state; }
public slots:
    void abortAndRetreat();
signals:
    void abortingAndRetreating();
};

#endif // GAMEINTERFACE_H
