#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include "engine.h"

class Player : public QObject
{
    Q_OBJECT
public:
    explicit Player(QObject *parent = nullptr);
    virtual ~Player() override;

signals:

public slots:
};

#endif // PLAYER_H
