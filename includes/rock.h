#ifndef ROCK_H
#define ROCK_H

#include "piece.h"

class Rock : public Piece
{
public:
    Rock(QPointF,bool,QGraphicsObject *);
    virtual ~Rock() override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override;
    virtual bool canMove(std::vector<Tile*>*,Tile * target = nullptr) const override;
    virtual int type() const override { return ROCK; }
};

#endif // ROCK_H
