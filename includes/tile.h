#ifndef TILE_H
#define TILE_H

#include <QGraphicsObject>
//#include "piece.h"

class Piece;
class Tile : public QGraphicsObject
{
    int _i,_j;
    bool _color;
    QBrush * _brush;
    QPen * _pen;
    QRectF _boundingRect;
    QSize _size;
    Piece * _piece;
    Tile * _adjacent[8];
public:
    Tile(int,int,int,int,bool,QGraphicsObject*);
    virtual ~Tile() override;
    bool isBlack() const { return _color; }
    QRectF boundingRect() const override;
    void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override;
    Piece* piece() const { return _piece; }
    void setPiece(Piece * p) { _piece = p; }
    int column() const { return _i; }
    int row() const { return _j; }
    Tile * adjacent(int i) const { return _adjacent[i]; }
    void setAdjacent(Tile*t,int i) { _adjacent[i] = t; }
};

#endif // TILE_H
