#ifndef BISHOP_H
#define BISHOP_H


#include "piece.h"

class Bishop : public Piece
{
public:
    Bishop(QPointF,bool,QGraphicsObject *);
    virtual ~Bishop() override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override;
    virtual bool canMove(std::vector<Tile*>*,Tile * target = nullptr) const override;
    virtual int type() const override { return BISHOP; }
};


#endif // BISHOP_H
