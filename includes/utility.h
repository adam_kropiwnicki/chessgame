#ifndef UTILITY_H
#define UTILITY_H
#include <QString>

QString IntToQString(int i);
int QStringToInt(QString s);

#endif // UTILITY_H
