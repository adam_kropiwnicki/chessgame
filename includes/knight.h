#ifndef KNIGHT_H
#define KNIGHT_H


#include "piece.h"

class Knight : public Piece
{
public:
    Knight(QPointF,bool,QGraphicsObject *);
    virtual ~Knight() override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *) override;
    virtual bool canMove(std::vector<Tile*>*,Tile * target = nullptr) const override;
    virtual int type() const override { return KNIGHT; }
};


#endif // KNIGHT_H
