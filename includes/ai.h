#ifndef AI_H
#define AI_H

#include <QObject>
#include "board.h"
#include "engine.h"
class AI : public QObject
{
    Q_OBJECT
    Board * board;
    bool color;
    int depth;
    Piece * chosenPiece;
    Tile * chosenTile;
    std::vector<Piece*> whiteOrder;
    std::vector<Piece*> blackOrder;
    double rate();
    double MinMax(double,double,int);
    int debug_historystartState;
public:
    AI(QObject *,Engine *);
    virtual ~AI();
signals:
    void requestMove(int,int,int,int,char);
public slots:
    void computeMove(Board *);
};

#endif // AI_H
