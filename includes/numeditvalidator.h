#ifndef EDITVALIDATOR_H
#define EDITVALIDATOR_H

#include<QValidator>

class NumEditValidator : public QValidator
{
    Q_OBJECT
    int _min,_max,_dig;
public:
    NumEditValidator(const int min, const int max,QObject *parent = Q_NULLPTR);
    virtual State validate(QString &input,int &pos) const;
    virtual void fixup(QString &input) const;
};

#endif // EDITVALIDATOR_H
