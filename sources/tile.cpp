#include "tile.h"
#include "configurtion.h"
#include <QPainter>

Tile::Tile(int x, int y, int a, int b, bool c, QGraphicsObject * parent)
    : QGraphicsObject(parent)
{
    _i = a;
    _j = b;
    setPos(x,y);
    _size.setHeight(Configuration::config().tile_size());
    _size.setWidth(Configuration::config().tile_size());
    _boundingRect.setTopLeft(QPointF(-1*(_size.width()/2),-1*(_size.height()/2)));
    _boundingRect.setSize(_size);
    _color = c;
    if (c) {
        _brush = new QBrush(Configuration::config().blacksColor());
        _pen = new QPen(Configuration::config().blacksColor(),1);
    }
    else {
        _brush = new QBrush(Configuration::config().whitesColor());
        _pen = new QPen(Configuration::config().whitesColor(),1);
    }
    _piece = nullptr;
    for (int i=0;i<8;i++) {
        _adjacent[i] = nullptr;
    }
}

Tile::~Tile()
{
    delete _brush;
    delete _pen;
}

QRectF Tile::boundingRect() const
{
   return _boundingRect;
}

void Tile::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{
    painter->setBrush(*_brush);
    painter->setPen(*_pen);
    painter->drawRect(_boundingRect);
}
