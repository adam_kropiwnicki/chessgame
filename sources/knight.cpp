#include "knight.h"
#include <QPainter>

/*---------------------------------------------------------------------------------------------------
 * ------------------------------------PUBLIC MEMBERS------------------------------------------------
 * ------------------------------------------------------------------------------------------------*/
Knight::Knight(QPointF p, bool c,QGraphicsObject *parent)
    : Piece(p,c,parent)
{
    if (c)
        _image = QPixmap(":/images/blackknight.png");
    else
        _image = QPixmap(":/images/whiteknight.png");
    _boundingRect = QRectF(QPointF(-1* (_image.width() /2),-1 * (_image.height()/2)),QSize(_image.width(),_image.height()));
}

Knight::~Knight()
{

}

QRectF Knight::boundingRect() const
{
    return _boundingRect;
}

void Knight::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{
    painter->drawPixmap(boundingRect().topLeft(),_image);
}

bool Knight::canMove(std::vector<Tile*>* moves,Tile * target) const
{
    if (tile()->adjacent(5)!=nullptr) {
        if (tile()->adjacent(5)->adjacent(6)!=nullptr) {
            if (tile()->adjacent(5)->adjacent(6)->piece()==nullptr || (tile()->adjacent(5)->adjacent(6)->piece()!=nullptr && tile()->adjacent(5)->adjacent(6)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(5)->adjacent(6));
        }
        if (tile()->adjacent(5)->adjacent(4)!=nullptr) {
            if (tile()->adjacent(5)->adjacent(4)->piece()==nullptr || (tile()->adjacent(5)->adjacent(4)->piece()!=nullptr && tile()->adjacent(5)->adjacent(4)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(5)->adjacent(4));
        }
    }
    if (tile()->adjacent(7)!=nullptr) {
        if (tile()->adjacent(7)->adjacent(6)!=nullptr) {
            if (tile()->adjacent(7)->adjacent(6)->piece()==nullptr || (tile()->adjacent(7)->adjacent(6)->piece()!=nullptr && tile()->adjacent(7)->adjacent(6)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(7)->adjacent(6));
        }
        if (tile()->adjacent(7)->adjacent(0)!=nullptr) {
            if (tile()->adjacent(7)->adjacent(0)->piece()==nullptr || (tile()->adjacent(7)->adjacent(0)->piece()!=nullptr && tile()->adjacent(7)->adjacent(0)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(7)->adjacent(0));
        }
    }
    if (tile()->adjacent(3)!=nullptr) {
        if (tile()->adjacent(3)->adjacent(4)!=nullptr) {
            if (tile()->adjacent(3)->adjacent(4)->piece()==nullptr || (tile()->adjacent(3)->adjacent(4)->piece()!=nullptr && tile()->adjacent(3)->adjacent(4)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(3)->adjacent(4));
        }
        if (tile()->adjacent(3)->adjacent(2)!=nullptr) {
            if (tile()->adjacent(3)->adjacent(2)->piece()==nullptr || (tile()->adjacent(3)->adjacent(2)->piece()!=nullptr && tile()->adjacent(3)->adjacent(2)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(3)->adjacent(2));
        }
    }
    if (tile()->adjacent(1)!=nullptr) {
        if (tile()->adjacent(1)->adjacent(0)!=nullptr) {
            if (tile()->adjacent(1)->adjacent(0)->piece()==nullptr || (tile()->adjacent(1)->adjacent(0)->piece()!=nullptr && tile()->adjacent(1)->adjacent(0)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(1)->adjacent(0));
        }
        if (tile()->adjacent(1)->adjacent(2)!=nullptr) {
            if (tile()->adjacent(1)->adjacent(2)->piece()==nullptr || (tile()->adjacent(1)->adjacent(2)->piece()!=nullptr && tile()->adjacent(1)->adjacent(2)->piece()->isBlack()!=isBlack()))
                moves->push_back(tile()->adjacent(1)->adjacent(2));
        }
    }
    if (target!=nullptr) {
        for (unsigned int i=0;i<moves->size();i++) {
            if ((*moves)[i] == target)
                return true;
        }
    }
    return false;
}
