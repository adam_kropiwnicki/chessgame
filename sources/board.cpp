#include "board.h"
#include "pawn.h"
#include "knight.h"
#include "rock.h"
#include "bishop.h"
#include "queen.h"
#include "king.h"
#include <iostream>
bool Board::initiateTiles()
{
    bool c = true;
    tiles = new Tile**[_cols];
    for (int i=0;i<_cols;i++) {
        tiles[i] = new Tile*[_rows];
        for (int j=0;j<_rows;j++) {
            tiles[i][j] = new Tile(_boundingRect.bottomLeft().rx()+(Configuration::config().tile_size()/2)+(i*Configuration::config().tile_size()),_boundingRect.bottomLeft().ry()-(Configuration::config().tile_size()/2)-(j*Configuration::config().tile_size()),i,j,c,this);
            c = !c;
        }
        if (tiles[i][0]->isBlack())
            c = false;
        else
            c = true;
    }
    for ( int i=0;i<_cols;i++) {                                        // set adjacents
        for ( int j=0;j<_rows;j++) {
            if (i+1 < _cols) {
                tiles[i][j]->setAdjacent(tiles[i+1][j],3);
                if (j+1 < _rows) {
                    tiles[i][j]->setAdjacent(tiles[i+1][j+1],4);
                }
                if (j-1 >= 0) {
                    tiles[i][j]->setAdjacent(tiles[i+1][j-1],2);
                }
            }
            if (i-1 >= 0) {
                tiles[i][j]->setAdjacent(tiles[i-1][j],7);
                if (j+1 < _rows) {
                    tiles[i][j]->setAdjacent(tiles[i-1][j+1],6);
                }
                if (j-1 >= 0) {
                    tiles[i][j]->setAdjacent(tiles[i-1][j-1],0);
                }
            }
            if (j+1 < _rows) {
                tiles[i][j]->setAdjacent(tiles[i][j+1],5);
            }
            if (j-1 >= 0) {
                tiles[i][j]->setAdjacent(tiles[i][j-1],1);
            }
        }
    }
    return true;
}

bool Board::initiatePieces()
{
    for (int i =0;i<_cols;i++) {
        for (int j=0;j<_rows;j++) {
            if (tiles != nullptr && tiles[i][j]->piece() == nullptr) {
                switch (Configuration::config().startPoint().pieces[i][j]) {
                case 'p':
                    createPiece(Piece::PAWN,0,tiles[i][j]);
                    break;
                case 'P':
                    createPiece(Piece::PAWN,1,tiles[i][j]);
                    break;
                case 'r':
                    createPiece(Piece::ROCK,0,tiles[i][j]);
                    break;
                case 'R':
                    createPiece(Piece::ROCK,1,tiles[i][j]);
                    break;
                case 'k':
                    createPiece(Piece::KNIGHT,0,tiles[i][j]);
                    break;
                case 'K':
                    createPiece(Piece::KNIGHT,1,tiles[i][j]);
                    break;
                case 'b':
                    createPiece(Piece::BISHOP,0,tiles[i][j]);
                    break;
                case 'B':
                    createPiece(Piece::BISHOP,1,tiles[i][j]);
                    break;
                case 'q':
                    createPiece(Piece::QUEEN,0,tiles[i][j]);
                    break;
                case 'Q':
                    createPiece(Piece::QUEEN,1,tiles[i][j]);
                    break;
                case 'g':
                    createPiece(Piece::KING,0,tiles[i][j]);
                    break;
                case 'G':
                    createPiece(Piece::KING,1,tiles[i][j]);
                    break;
                case '-':
                    break;
                }
            }
            else {
                std::cout<<"error in switch\n"<<i<<'\n'<<j<<'\n';
                return false;
            }
        }
    }
    return true;
}

void Board::move(Move m)
{
    //DEBUG SECTION KURWO
    if (!history.empty())
    {
        if (history.back().piece->isBlack()==m.piece->isBlack()) {
            if (history.back().piece->type() == Piece::ROCK && m.piece->type()==Piece::KING ) {
                //castle - rather normal
            }
            else {
                std::cout<<"Debug error - turn count problem\n";
            }
        }
    }
    //END OF DEBUG SECTION PIZDO

    m.piece->tile()->setPiece(nullptr);
    m.piece->setTile(m.target);
    if (m.taken != nullptr) {
        removePiece(m.taken);
    }
    m.target->setPiece(m.piece);
    history.push_back(m);
    m.piece->justMoved();
    if (m.piece->tile()->piece() != m.piece) {                                          //TESTING & DEBUGING - ASSERTION
        std::cout<<"move exception; m.piece disassebled\n";                             //
    }                                                                                   //
    if (m.from->piece() != nullptr) {                                                   //
        std::cout<<"move exception; m.from disassebled (not pointing to null) \n";      //
    }
    _turn = !_turn;
}

void Board::undoMove()
{
    if (history.empty()) {
        std::cout<<"Tried to undo move, but history is empty\n";
        return;
    }
    Move m = history.back();
    history.pop_back();

    if (!m.piece->isVisible()) {                        // removing & deleting piece made from promotion
        if (m.target->piece() == nullptr) {
            std::cout<<"undo move exception; promoted piece not standing on his promoton tile\n";       // DEBUG
        }
        removePiece(m.target->piece());
        delete m.target->piece();
        m.piece->show();
    }

    if (m.taken!=nullptr) {
      //  if (m.taken->tile() != m.target) {                                      //TESTING & DEBUGING - ASSERTION
      //      std::cout<<"undo move exception; taken not pointing on target\n";   //
      //  }                                                                       //
        m.taken->tile()->setPiece(m.taken);
        if (m.taken->tile() != m.target)
            m.target->setPiece(nullptr);
        m.taken->show();
        if (m.taken->isBlack()) {
             blacks.push_back(m.taken);
        }
        else {
             whites.push_back(m.taken);
        }
    }
    else
        m.target->setPiece(nullptr);
    m.piece->setTile(m.from);
    m.from->setPiece(m.piece);
    m.piece->justUndoMoved();
    if (!history.empty() && (history.back().piece->isBlack() == m.piece->isBlack()) && m.piece->type() == Piece::KING && history.back().piece->type() == Piece::ROCK) {
        int i = m.target->column() - m.from->column();
        if (i == 2 || i == -2) {
            Move v = history.back();
            undoMove();
            _turn = !_turn;
            undoGraphicMove(v);
        }
    }
    undoGraphicMove(m);
    _turn = !_turn;
    if (m.piece->tile()->piece() != m.piece) {                                          //TESTING & DEBUGING - ASSERTION
        std::cout<<"undo move exception; m.piece disassebled\n";                        //
    }                                                                                   //
    if (m.taken != nullptr && m.taken->tile()->piece() != m.taken) {                    //
        std::cout<<"undo move exception; m.taken disassebled\n";                        //
    }                                                                                   //
    if (m.from->piece()->tile() != m.from) {                                            //
        std::cout<<"undo move exception; m.from disassebled\n";                         //
    }                                                                                   //
    if (m.taken == nullptr && m.target->piece() != nullptr) {                           //
        std::cout<<"undo move exception; m.target disassebled (not take)\n";            //
    }                                                                                   //
}

void Board::graphicMove(Move m)
{
    m.piece->moveBy(m.target->pos().x()-m.piece->pos().x(),m.target->pos().y()-m.piece->pos().y());
}

void Board::undoGraphicMove(Move m)
{
    m.piece->moveBy(m.from->pos().x()-m.piece->pos().x(),m.from->pos().y()-m.piece->pos().y());
}

void Board::anchorPiece(Piece *p)
{
    if (p->tile() != nullptr && p->tile()->piece() == p)
        p->moveBy(p->tile()->pos().x()-p->pos().x(),p->tile()->pos().y()-p->pos().y());
    else {
        //error report - anchoring on a null-tile or tile possesed by other piece
    }
}

void Board::createPiece(int typ, bool side, Tile * t)
{
    if (t->piece() != nullptr)
        removePiece(t->piece());                        // possible info loss and memory leak
    if (side) {
        switch (typ) {
        case Piece::PAWN:
            blacks.push_back(new Pawn(t->pos(),1,this));
            t->setPiece(blacks.back());
            t->piece()->setTile(t);
            connect(blacks.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::KNIGHT:
            blacks.push_back(new Knight(t->pos(),1,this));
            t->setPiece(blacks.back());
            t->piece()->setTile(t);
            connect(blacks.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::BISHOP:
            blacks.push_back(new Bishop(t->pos(),1,this));
            t->setPiece(blacks.back());
            t->piece()->setTile(t);
            connect(blacks.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::ROCK:
            blacks.push_back(new Rock(t->pos(),1,this));
            t->setPiece(blacks.back());
            t->piece()->setTile(t);
            connect(blacks.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::QUEEN:
            blacks.push_back(new Queen(t->pos(),1,this));
            t->setPiece(blacks.back());
            t->piece()->setTile(t);
            connect(blacks.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::KING:
            if (_blackKing != nullptr) {
                //report error - King already exists
                std::cout<<"King already exist?\n";
                return;
            }
            blacks.push_back(new King(t->pos(),1,this));
            t->setPiece(blacks.back());
            t->piece()->setTile(t);
            connect(blacks.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            _blackKing = blacks.back();
            break;
        default:
            std::cout<<"WTF? IT DOESNT EXIST\n";
            break;
        }
    }
    else {
        switch (typ) {
        case Piece::PAWN:
            whites.push_back(new Pawn(t->pos(),0,this));
            t->setPiece(whites.back());
            t->piece()->setTile(t);
            connect(whites.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::KNIGHT:
            whites.push_back(new Knight(t->pos(),0,this));
            t->setPiece(whites.back());
            t->piece()->setTile(t);
            connect(whites.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::BISHOP:
            whites.push_back(new Bishop(t->pos(),0,this));
            t->setPiece(whites.back());
            t->piece()->setTile(t);
            connect(whites.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::ROCK:
            whites.push_back(new Rock(t->pos(),0,this));
            t->setPiece(whites.back());
            t->piece()->setTile(t);
            connect(whites.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::QUEEN:
            whites.push_back(new Queen(t->pos(),0,this));
            t->setPiece(whites.back());
            t->piece()->setTile(t);
            connect(whites.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            break;
        case Piece::KING:
            if (_whiteKing != nullptr) {
                //error report - King already exists
                std::cout<<"King already exist?\n";
                return;
            }
            whites.push_back(new King(t->pos(),0,this));
            t->setPiece(whites.back());
            t->piece()->setTile(t);
            connect(whites.back(),SIGNAL(moving(Piece*,QPointF)),this,SLOT(moveRequested(Piece*,QPointF)));
            _whiteKing = whites.back();
            break;
        }
    }
}

void Board::removePiece(Piece *p)
{
    if (p->isBlack()) {                     // removing from 'color' containers
        for (unsigned int i=0;i<blacks.size();i++){
            if (p==blacks[i]){
                blacks.erase(blacks.begin()+i);
            }
        }
    }
    else {
        for (unsigned int i=0;i<whites.size();i++){
            if (p==whites[i]){
                whites.erase(whites.begin()+i);
            }
        }
    }
    p->tile()->setPiece(nullptr);
    p->hide();
}

void Board::resetBoard()
{
/*    for (int i=0;i<_cols;i++) {
        for (int j=0;j<_rows;j++) {
            if (tiles[i][j]->piece() != nullptr) {
                delete tiles[i][j]->piece();
                tiles[i][j]->setPiece(nullptr);
            }
        }
    } */
    for (int i=0;i<_cols;i++) {
        for (int j=0;j<_rows;j++) {
            tiles[i][j]->setPiece(nullptr);
        }
    }
    for (unsigned int i=0;i<blacks.size();i++) {
        delete blacks[i];
    }
    for (unsigned int i=0;i<whites.size();i++) {
        delete whites[i];
    }
    whites.clear();
    blacks.clear();
    history.clear();
    _blackKing = nullptr;
    _whiteKing = nullptr;
    _turn = Configuration::config().startPoint().startingPlayer;
}

void Board::restoreBoard(Board * brd)
{
    resetBoard();
    for (int i=0;i<_cols;i++) {
        for (int j=0;j<_rows;j++) {
            if (brd->tiles[i][j]->piece() != nullptr) {
                createPiece(brd->tiles[i][j]->piece()->type(),brd->tiles[i][j]->piece()->isBlack(),tiles[i][j]);
                for (int k=0;k<brd->tiles[i][j]->piece()->hasMoved();k++) {
                    tiles[i][j]->piece()->justMoved();
                }
            }
        }
    }
    _turn = brd->turn();
}

Tile *Board::tileAt(QPointF p) const
{
    p = mapFromScene(p);
    int i = (p.x()+((Configuration::config().tile_size()*_cols)/2))/Configuration::config().tile_size();
    int j = (p.y()+((Configuration::config().tile_size()*_rows)/2))/Configuration::config().tile_size();
    if (i<_rows && j<_cols) {
        if (i>=0 && j>=0)
            return tiles[i][_rows-1-j];
    }
    return nullptr;
}


/*---------------------------------------------------------------------
 * --------------------------PUBLIC FUNCTIONS--------------------------
 * ------------------------------------------------------------------*/
Board::Board()
    : QGraphicsObject(),
      _rows(Configuration::config().rows()),
      _cols(Configuration::config().cols())
{
    tiles = nullptr;
    _whiteKing = nullptr;
    _blackKing = nullptr;
    _ok = false;
    _endgame = false;
    _size.setHeight(Configuration::config().rows() * Configuration::config().tile_size());
    _size.setWidth(Configuration::config().cols() * Configuration::config().tile_size());
    _boundingRect.setTopLeft(QPointF(-1*(_size.width()/2),-1*(_size.height()/2)));
    _boundingRect.setSize(_size);
    current = new Situation(_cols,_rows);
    _turn = Configuration::config().startPoint().startingPlayer;
    initiateTiles();
    initiatePieces();
}

Board::~Board()
{
    for (int i=0;i<Configuration::config().cols();i++) {
        for (int j=Configuration::config().rows() - 1;j>=0;j--) {
            delete tiles[i][j];
        }
        delete[] tiles[i];
    }
    delete[] tiles;
    delete current;
}

QRectF Board::boundingRect() const
{
    return _boundingRect;
}

void Board::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{

}

Situation Board::situation() const
{
    char c;
    for (int i =0; i<_cols; i++) {
        for (int j=0; j<_rows; j++) {
            if (tiles[i][j]->piece()!=nullptr) {
                switch (tiles[i][j]->piece()->type()) {
                case Piece::PAWN:
                    c = 'P';
                    break;
                 case Piece::KNIGHT:
                    c = 'K';
                    break;
                 case Piece::BISHOP:
                    c = 'B';
                    break;
                 case Piece::ROCK:
                    c = 'R';
                    break;
                 case Piece::QUEEN:
                    c = 'Q';
                    break;
                 case Piece::KING:
                    c = 'G';
                    break;
                 default:
                    break;
                }
                if (!tiles[i][j]->piece()->isBlack())
                    c = c + 32;
            }
            else
                c = '-';
            current->pieces[i][j] = c;
        }
    }
    return *current;
}

void Board::moveRequested(Piece *p,QPointF pnt)
{
    Tile * target = tileAt(pnt);
    if (target == nullptr) {
        anchorPiece(p);
        return;
    }
    Move m = Move(p,target);
    emit callEngine(this,m);
    if (_ok) {
        if (history.size() > 200 ) {
            emit gameOver(2);
        }
        if (p->isBlack()) {
            if (Configuration::config().whiteController() == Configuration::HUMAN) {
                emit commitMove(m);
            }
            if (_endgame)
                emit gameOver(true);
            if (Configuration::config().whiteController() == Configuration::AI) {
                emit callAI(this);
            }
        }
        else {
            if (Configuration::config().blackController() == Configuration::HUMAN) {
                emit commitMove(m);
            }
            if (_endgame)
                emit gameOver(false);
            if (Configuration::config().blackController() == Configuration::AI) {
                emit callAI(this);
            }
        }
    }
    return;
}

void Board::moveRequested(int from_x, int from_y, int target_x, int target_y, char prom)
{
    if (from_x < 0 || from_y < 0 || target_x < 0 || target_y < 0) {
        //error report?
        std::cout<<"Attempt to move from or onto nonexisting tile\n";
        _ok = false;
        return;
    }
    if (from_x >= _cols || from_y >= _rows || target_x >= _cols || target_y >= _rows) {
        //error report?
        std::cout<<"Attempt to move from or onto nonexisting tile\n";
        _ok = false;
        return;
    }
    if (tiles[from_x][from_y]->piece() == nullptr) {
        //error report?
        std::cout<<"Attempt to move a nonexisting piece\n";
        _ok = false;
        return;
    }
    Move m(tiles[from_x][from_y]->piece(),tiles[target_x][target_y]);
    *m.promotion = prom;
    emit callEngine(this,m);
    if (_ok) {
        if (history.size() > 200 ) {
            return;
            emit gameOver(2);
        }
        if (m.piece->isBlack()) {
            if (Configuration::config().whiteController() == Configuration::HUMAN) {
                emit commitMove(m);
            }
            if (_endgame)
                emit gameOver(1);
            if (Configuration::config().whiteController() == Configuration::AI) {
                emit callAI(this);
            }
        }
        else {
            if (Configuration::config().blackController() == Configuration::HUMAN) {
                emit commitMove(m);
            }
            if (_endgame)
                emit gameOver(0);
            if (Configuration::config().blackController() == Configuration::AI) {
                emit callAI(this);
            }
        }
    }
    return;
}

