#include "client.h"
#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include "utility.h"
#include <iostream>


Client::Client(QWidget *parent)
    : QWidget(parent),
      socket(new QTcpSocket(this))
{
    in.setDevice(socket);
    in.setVersion(QDataStream::Qt_4_0);
    interfaceDialog = new QDialog(this);
    QGridLayout * layout = new QGridLayout(interfaceDialog);
    QLabel * ipLabel = new QLabel("Host IP address:",interfaceDialog);
    QLabel * portLabel = new QLabel("Port:",interfaceDialog);
    ipEdit = new QLineEdit(this);
    portEdit = new QLineEdit(this);
    errorDialog = new QMessageBox(QMessageBox::Critical,"Error","Cannot connect to host",QMessageBox::Ok);
    QPushButton * confirmButton = new QPushButton("Confirm",interfaceDialog);
    layout->addWidget(ipLabel,0,0);
    layout->addWidget(portLabel,1,0);
    layout->addWidget(ipEdit,0,1);
    layout->addWidget(portEdit,1,1);
    layout->addWidget(confirmButton,2,1);
    interfaceDialog->setLayout(layout);

    connect(confirmButton,SIGNAL(clicked(bool)),this,SLOT(makeConnection()));
    connect(socket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(errorHandling(QAbstractSocket::SocketError)));
    connect(socket,SIGNAL(connected()),interfaceDialog,SLOT(accept()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(decryptMessage()));

    connect(this,SIGNAL(setConfigurationCols(int)),&(Configuration::config()),SLOT(set_rows(int)));
    connect(this,SIGNAL(setConfigurationRows(int)),&(Configuration::config()),SLOT(set_cols(int)));
    connect(this,SIGNAL(setConfigurationTile_size(int)),&(Configuration::config()),SLOT(set_tileSize(int)));
    connect(this,SIGNAL(setConfigurationStartPoint(Situation)),&(Configuration::config()),SLOT(set_StartingPoint(Situation)));
    connect(this,SIGNAL(setConfigurationBlackController(int)),&(Configuration::config()),SLOT(set_blackController(int)));
    connect(this,SIGNAL(setConfigurationWhiteController(int)),&(Configuration::config()),SLOT(set_whiteController(int)));
    return;
}

Client::~Client()
{
    socket->disconnectFromHost();
    socket->close();
}

bool Client::makeConnection()
{
    socket->connectToHost(ipEdit->text(),portEdit->text().toInt());
    return true;
}

void Client::errorHandling(QAbstractSocket::SocketError err)
{
    switch (err) {
    case 0:
        errorDialog->setText("The connection was refused by the peer (or timed out).");
        break;
    case 1:
        errorDialog->setText("The remote host closed the connection. Note that the client socket (i.e., this socket) will be closed after the remote close notification has been sent.");
        break;
    case 2:
        errorDialog->setText("The host address was not found.");
        break;
    case 3:
        errorDialog->setText("The socket operation failed because the application lacked the required privileges.");
        break;
    case 4:
        errorDialog->setText("The local system ran out of resources (e.g., too many sockets)");
        break;
    case 5:
        errorDialog->setText("The socket operation timed out.");
        break;
    case 6:
        errorDialog->setText("The datagram was larger than the operating system's limit (which can be as low as 8192 bytes)");
        break;
    case 7:
        errorDialog->setText("An error occurred with the network (e.g., the network cable was accidentally plugged out).");
        break;
    case 8:
        errorDialog->setText("The address specified to QAbstractSocket::bind() is already in use and was set to be exclusive.");
        break;
    case 9:
        errorDialog->setText("The address specified to QAbstractSocket::bind() does not belong to the host.");
        break;
    case 10:
        errorDialog->setText("The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support).");
        break;
    case 11:
        errorDialog->setText("Used by QAbstractSocketEngine only, The last operation attempted has not finished yet (still in progress in the background).");
        break;
    case 12:
        errorDialog->setText("The socket is using a proxy, and the proxy requires authentication.");
        break;
    case 13:
        errorDialog->setText("The SSL/TLS handshake failed, so the connection was closed (only used in QSslSocket)");
        break;
    case 14:
        errorDialog->setText("Could not contact the proxy server because the connection to that server was denied");
        break;
    case 15:
        errorDialog->setText("The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)");
        break;
    case 16:
        errorDialog->setText("The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase.");
        break;
    case 17:
        errorDialog->setText("The proxy address set with setProxy() (or the application proxy) was not found.");
        break;
    case 18:
        errorDialog->setText("The connection negotiation with the proxy server failed, because the response from the proxy server could not be understood.");
        break;
    case 19:
        errorDialog->setText("An operation was attempted while the socket was in a state that did not permit it.");
        break;
    case 20:
        errorDialog->setText("The SSL library being used reported an internal error. This is probably the result of a bad installation or misconfiguration of the library.");
        break;
    case 21:
        errorDialog->setText("Invalid data (certificate, key, cypher, etc.) was provided and its use resulted in an error in the SSL library.");
        break;
    case 22:
        errorDialog->setText("A temporary error occurred (e.g., operation would block and socket is non-blocking).");
        break;
    default:
        errorDialog->setText("An unidentified error occurred.");
        break;
    }
    errorDialog->exec();
}

void Client::decryptMessage()
{
    in.startTransaction();

    QString message;
    in >> message;

    if (!in.commitTransaction())
        return;
    if (message[0] == 'C') {

        bool intDecoding = false;
        bool sitDecoding = false;
        QString intTmp;
        QString sitTmp;
        int values[5] = { 0, 0, 0, 0, 0 };
        int j=0;

        for (int i=1;i<message.size();i++){
            if (message[i]=='X') {
                if (intDecoding) {
                    intDecoding = false;
                    values[j] = intTmp.toInt();
                    j++;
                    intTmp.clear();
                }
                continue;
            }
            if (message[i] == 'S') {
                sitDecoding = true;
                intDecoding = false;
                continue;
            }
            if (message[i] == 'E') {
                sitDecoding = false;
                continue;
            }
            if (message[i].isDigit()) {
                intDecoding = true;
                intTmp += message[i];
                continue;
            }
            if (sitDecoding && !intDecoding) {
                sitTmp += message[i];
            }
        }
        for (j=0;j<5;j++) {
            if (values[j] == 0) {
                std::cout<<"Error in decrypting";
                //emit an error signal???
                return;
            }
        }
        Situation sit(values[0],values[1]);
        if (!sit.fromQString(sitTmp)) {
            //emit an errr signal?
            return;
        }
        emit setConfigurationStartPoint(sit);
        emit setConfigurationCols(values[0]);
        emit setConfigurationRows(values[1]);
        emit setConfigurationTile_size(values[2]);
        if (values[4] == Configuration::HUMAN)
            emit setConfigurationBlackController(Configuration::YOU);
        if (values[4] == Configuration::YOU)
            emit setConfigurationBlackController(Configuration::HUMAN);
        if (values[4] == Configuration::AI)
            emit setConfigurationBlackController(Configuration::HUMAN);
        if (values[3] == Configuration::HUMAN)
            emit setConfigurationWhiteController(Configuration::YOU);
        if (values[3] == Configuration::YOU)
            emit setConfigurationWhiteController(Configuration::HUMAN);
        if (values[3] == Configuration::AI)
            emit setConfigurationWhiteController(Configuration::HUMAN);
        return;
    }
    if (message[0] == 'M') {
        bool intDecoding = false;
        QString intTmp;
        char promotion;
        int values[4] = { -1, -1, -1, -1 };
        int j=0;

        for (int i=1;i<message.size();i++){
            if (message[i]=='X') {
                if (intDecoding) {
                    intDecoding = false;
                    values[j] = intTmp.toInt();
                    j++;
                    intTmp.clear();
                }
                continue;
            }
            if (message[i].isDigit()) {
                intDecoding = true;
                intTmp += message[i];
                continue;
            }
            else {
                promotion = message[i].toLatin1();
            }
        }
        for (j=0;j<4;j++) {
            if (values[j] == -1) {
                std::cout<<"Error in decrypting";
                //emit an error signal???
                return;
            }
        }
        emit requestMove(values[0],values[1],values[2],values[3],promotion);
        return;
    }
    return;
}

void Client::sendMove(Move m)
{
    QByteArray block;
    QDataStream out(&block,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out <<'M'+IntToQString(m.from->column())+'X'+IntToQString(m.from->row())+'X'+IntToQString(m.target->column())+'X'+IntToQString(m.target->row())+'X'+*m.promotion+'X';
    if (socket != 0) {
        socket->write(block);
    }
    return;
}

void Client::run()
{
    if (interfaceDialog->exec() == QDialog::Rejected) {
        emit goBack();
    }

    return;
}
