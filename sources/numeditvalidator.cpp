#include "numeditvalidator.h"
#include "utility.h"

NumEditValidator::NumEditValidator(const int min,const int max,QObject *parent)
    : QValidator(parent)
{
    _min = min;
    _max = max;
    _dig = 1;
    int i = max;
    while (i/10 > 0) {
        _dig++;
        i = i/10;
    }
}

QValidator::State NumEditValidator::validate(QString &input, int &pos) const
{
    State state = QValidator::Acceptable;
    if (input.length() == 0)
        state = QValidator::Intermediate;
    else {
        if (input.length()>_dig)
            state = QValidator::Invalid;
        else {
            bool ok = true;
            int num = input.toInt(&ok);
            if (!ok)
                state = QValidator::Invalid;
            else {
                if (num > _max || num < _min)
                    state = QValidator::Intermediate;
            }
        }
    }
    return state;
}

void NumEditValidator::fixup(QString &input) const
{
    bool ok = true;
    int num = input.toInt(&ok);
    if(!ok) {

    }
    else {
        if ( num < _min )
            input = IntToQString(_min);
        if ( num > _max )
            input = IntToQString(_max);
    }
}
