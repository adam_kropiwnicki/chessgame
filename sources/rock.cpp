#include "rock.h"
#include <QPainter>

/*---------------------------------------------------------------------------------------------------
 * ------------------------------------PUBLIC MEMBERS------------------------------------------------
 * ------------------------------------------------------------------------------------------------*/
Rock::Rock(QPointF p, bool c,QGraphicsObject *parent)
    : Piece(p,c,parent)
{
    if (c)
        _image = QPixmap(":/images/blackrock.png");
    else
        _image = QPixmap(":/images/whiterock.png");
    _boundingRect = QRectF(QPointF(-1* (_image.width() /2),-1 * (_image.height()/2)),QSize(_image.width(),_image.height()));
}

Rock::~Rock()
{

}

QRectF Rock::boundingRect() const
{
    return _boundingRect;
}

void Rock::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{
    painter->drawPixmap(boundingRect().topLeft(),_image);
}

bool Rock::canMove(std::vector<Tile*>* moves,Tile * target) const
{
    Tile * tmp = tile();
    for (int i=1; i<8; i+=2) {
        while (tmp->adjacent(i)!=nullptr) {
            if (tmp->adjacent(i)->piece()!=nullptr && tmp->adjacent(i)->piece()->isBlack() == isBlack())
                break;
            moves->push_back(tmp->adjacent(i));
            if (tmp->adjacent(i)->piece()!=nullptr && tmp->adjacent(i)->piece()->isBlack() != isBlack())
                break;
            tmp = tmp->adjacent(i);
        }
        tmp = tile();
    }
    if (target!=nullptr) {
        for (unsigned int i=0;i<moves->size();i++) {
            if ((*moves)[i] == target)
                return true;
        }
    }
    return false;
}
