#include "engine.h"

Engine::Engine(QWidget *parent)
    : QObject(parent)
{
    w_promotionDialog = new QMessageBox(QMessageBox::NoIcon,"Promotion dialog","Choose your promotion:",QMessageBox::NoButton,parent);      // Initiating promotion dialogs and their buttons
    b_promotionDialog = new QMessageBox(QMessageBox::NoIcon,"Promotion dialog","Choose your promotion:",QMessageBox::NoButton,parent);
    QPushButton * w_knightPick = w_promotionDialog->addButton("",QMessageBox::AcceptRole);
    QPushButton * w_bishopPick = w_promotionDialog->addButton("",QMessageBox::AcceptRole);
    QPushButton * w_rockPick = w_promotionDialog->addButton("",QMessageBox::AcceptRole);
    QPushButton * w_queenPick = w_promotionDialog->addButton("",QMessageBox::AcceptRole);
    QPushButton * b_knightPick = b_promotionDialog->addButton("",QMessageBox::AcceptRole);
    QPushButton * b_bishopPick = b_promotionDialog->addButton("",QMessageBox::AcceptRole);
    QPushButton * b_rockPick = b_promotionDialog->addButton("",QMessageBox::AcceptRole);
    QPushButton * b_queenPick = b_promotionDialog->addButton("",QMessageBox::AcceptRole);
    w_promotionButtons.push_back(w_queenPick);
    w_promotionButtons.push_back(w_knightPick);
    w_promotionButtons.push_back(w_bishopPick);
    w_promotionButtons.push_back(w_rockPick);
    b_promotionButtons.push_back(b_queenPick);
    b_promotionButtons.push_back(b_knightPick);
    b_promotionButtons.push_back(b_bishopPick);
    b_promotionButtons.push_back(b_rockPick);
    w_promotionDialog->setDefaultButton(w_queenPick);
    b_promotionDialog->setDefaultButton(b_queenPick);

    QPixmap tmp_pix(":/images/blackqueen.png");                                                                                             // customalizing promotion buttons
    QIcon tmp_icon(tmp_pix);
    b_queenPick->setIcon(tmp_icon);
    b_queenPick->setIconSize(tmp_pix.rect().size());
    tmp_pix = QPixmap(":/images/whitequeen.png");
    tmp_icon = QIcon(tmp_pix);
    w_queenPick->setIcon(tmp_icon);
    w_queenPick->setIconSize(tmp_pix.rect().size());

    tmp_pix = QPixmap(":/images/blackrock.png");
    tmp_icon = QIcon(tmp_pix);
    b_rockPick->setIcon(tmp_icon);
    b_rockPick->setIconSize(tmp_pix.rect().size());
    tmp_pix = QPixmap(":/images/whiterock.png");
    tmp_icon = QIcon(tmp_pix);
    w_rockPick->setIcon(tmp_icon);
    w_rockPick->setIconSize(tmp_pix.rect().size());

    tmp_pix = QPixmap(":/images/blackbishop.png");
    tmp_icon = QIcon(tmp_pix);
    b_bishopPick->setIcon(tmp_icon);
    b_bishopPick->setIconSize(tmp_pix.rect().size());
    tmp_pix = QPixmap(":/images/whitebishop.png");
    tmp_icon = QIcon(tmp_pix);
    w_bishopPick->setIcon(tmp_icon);
    w_bishopPick->setIconSize(tmp_pix.rect().size());

    tmp_pix = QPixmap(":/images/blackknight.png");
    tmp_icon = QIcon(tmp_pix);
    b_knightPick->setIcon(tmp_icon);
    b_knightPick->setIconSize(tmp_pix.rect().size());
    tmp_pix = QPixmap(":/images/whiteknight.png");
    tmp_icon = QIcon(tmp_pix);
    w_knightPick->setIcon(tmp_icon);
    w_knightPick->setIconSize(tmp_pix.rect().size());
}

Engine::~Engine()
{
    delete w_promotionDialog;
    delete b_promotionDialog;
}

bool Engine::processMove(Board * brd, Move m)
{
    if (m.piece->isBlack()!=brd->_turn && (m.target->piece()==nullptr || m.target->piece()->type()!=Piece::KING)) {
        brd->undoGraphicMove(m);
        brd->_ok = false;
        return false;
    }
    std::vector<Tile*> *movelist = new std::vector<Tile*>();
    if (m.piece->canMove(movelist,m.target)) {

    }
    else { //impossible  move
        brd->undoGraphicMove(m);
        delete movelist;
        brd->_ok = false;
        return false;
    }

    if (m.piece->type()==Piece::KING && (m.target->column() - m.from->column() == 2 || m.target->column() - m.from->column() == -2)) {   // CHECK IF TRYING TO CASTLE
        if (checkforcheck(brd,!brd->_turn)) {                                                                                                // CHECK IF KING IS CHECKED
            brd->undoGraphicMove(m);
            delete movelist;
            brd->_ok = false;
            return false;
        }
        int i = m.target->column() - m.from->column();
        Move tmp = Move(m.piece,brd->tiles[m.from->column() + (i/2)][m.target->row()]);
        if(!processMove(brd,tmp)) {                                                                                                         // CHECK IF KING IS TRYING TO JUMP OVER CHECKED TILE
            brd->undoGraphicMove(m);
            delete movelist;
            brd->_ok = false;
            return false;
        }
        brd->undoMove();
        //brd->_turn = !brd->_turn;
        brd->undoGraphicMove(m);
        if ( i == 2 ) {                                                                                                                     // DO A ROCK MOVE
            tmp = Move(m.target->adjacent(3)->piece(),tmp.target);
            brd->move(tmp);
            brd->graphicMove(tmp);
            brd->_turn = !brd->_turn;
        }
        if ( i == -2 ) {
            tmp = Move(m.target->adjacent(7)->adjacent(7)->piece(),tmp.target);
            brd->move(tmp);
            brd->graphicMove(tmp);
            brd->_turn = !brd->_turn;
        }
    }

    if (m.piece->type()==Piece::PAWN && m.taken == nullptr && m.piece->tile()->column() != m.target->column()) {                            // Bicie w przelocie
        if (brd->history.back().piece == brd->tiles[m.target->column()][m.piece->tile()->row()]->piece()) {
            m.taken = brd->tiles[m.target->column()][m.piece->tile()->row()]->piece();
            brd->removePiece(brd->tiles[m.target->column()][m.piece->tile()->row()]->piece());
        }
        else {
            brd->undoGraphicMove(m);
            delete movelist;
            brd->_ok = false;
            return false;
        }
    }

    if (m.target->piece()!=nullptr && m.target->piece()->type() == Piece::KING) {
        // Avoiding loops in searching for checks
    }
    else {
        brd->move(m);
        if (checkforcheck(brd,brd->_turn)) {
            brd->undoMove();
            //brd->_turn = !brd->_turn;
            brd->undoGraphicMove(m);
            delete movelist;
            brd->_ok = false;
            return false;
        }
        brd->graphicMove(m);

        if (m.piece->type() == Piece::PAWN && (m.target->row() == 0 || m.target->row() == 7)) {                                         // CHECK IF PAWN IS PROMOTING
            if (*m.promotion == '-') {
                if (m.piece->isBlack()) {
                    b_promotionDialog->exec();
                    for (unsigned int i=0;i<b_promotionButtons.size();i++) {
                        if (b_promotionButtons[i]==b_promotionDialog->clickedButton()) {
                            switch (i) {
                            case 0:
                                brd->createPiece(Piece::QUEEN,1,m.target);
                                *m.promotion = 'q';
                                break;
                            case 1:
                                brd->createPiece(Piece::KNIGHT,1,m.target);
                                *m.promotion = 'k';
                                break;
                            case 2:
                                brd->createPiece(Piece::BISHOP,1,m.target);
                                *m.promotion = 'b';
                                break;
                            case 3:
                                brd->createPiece(Piece::ROCK,1,m.target);
                                *m.promotion = 'r';
                                break;
                            default:
                                //error report - undefined button in promotion vector
                                break;
                            }
                        }
                        else {
                            //error report - undefined promotion button
                        }
                    }
                }
                else {
                    w_promotionDialog->exec();
                    for (unsigned int i=0;i<w_promotionButtons.size();i++) {
                        if (w_promotionButtons[i]==w_promotionDialog->clickedButton()) {
                            switch (i) {
                            case 0:
                                brd->createPiece(Piece::QUEEN,0,m.target);
                                *m.promotion = 'q';
                                break;
                            case 1:
                                brd->createPiece(Piece::KNIGHT,0,m.target);
                                *m.promotion = 'k';
                                break;
                            case 2:
                                brd->createPiece(Piece::BISHOP,0,m.target);
                                *m.promotion = 'b';
                                break;
                            case 3:
                                brd->createPiece(Piece::ROCK,0,m.target);
                                *m.promotion = 'r';
                                break;
                            default:
                                //error report - undefined button in promotion vector
                                break;
                            }
                        }
                        else {
                            //error report - undefined promotion button
                        }
                    }
                }
            }
            else {
                int color;
                if (m.piece->isBlack())
                    color = 1;
                else
                    color = 0;
                switch(*m.promotion) {
                case 'q':
                    brd->createPiece(Piece::QUEEN,color,m.target);
                    break;
                case 'b':
                    brd->createPiece(Piece::BISHOP,color,m.target);
                    break;
                case 'k':
                    brd->createPiece(Piece::KNIGHT,color,m.target);
                    break;
                case 'r':
                    brd->createPiece(Piece::ROCK,color,m.target);
                    break;
                default:
                    break;
                }
            }
        }
        
        if (checkforcheck(brd,!brd->_turn)) {                                                                                    // CHECKMATE CHECK
            if (checkforcheckmate(brd,brd->_turn)) {
                brd->_endgame = true;
            }
            else
                brd->_endgame = false;
        }
        else {
            brd->_endgame = false;
        }
    }
    delete movelist;
    brd->_ok = true;
    return true;
}

bool Engine::checkforcheck(Board *brd,bool side)  //checkin if side person iss being checked
{
    if (side) {
        for (unsigned long i=0;i<brd->blacks.size();i++) {
            if (processMove(brd,Move(brd->blacks[i],brd->whiteKing()->tile()))) {
                return true;
            }
        }
    }
    else {
        for (unsigned long i=0;i<brd->whites.size();i++) {
            if (processMove(brd,Move(brd->whites[i],brd->blackKing()->tile()))) {
                return true;
            }
        }
    }

    return false;
}

bool Engine::checkforcheckmate(Board *brd, bool side)
{
    std::vector<Tile*>* movelist = new std::vector<Tile*>();
    if (side) {
        for (unsigned int i=0; i<brd->blacks.size();i++) {
            brd->blacks[i]->canMove(movelist);
            for (unsigned int j=0; j<movelist->size();j++) {
                Move m = Move(brd->blacks[i],(*movelist)[j]);
                if(processMove(brd,m)) {
                    brd->undoMove();
                    brd->undoGraphicMove(m);
                    //brd->_turn = !brd->_turn;
                    delete movelist;
                    return false;
                }
            }
            movelist->clear();
        }
    }
    else {
        for (unsigned int i=0; i<brd->whites.size();i++) {
            brd->whites[i]->canMove(movelist);
            for (unsigned int j=0; j<movelist->size();j++) {
                Move m = Move(brd->whites[i],(*movelist)[j]);
                if(processMove(brd,m)) {
                    brd->undoMove();
                    brd->undoGraphicMove(m);
                   // brd->_turn = !brd->_turn;
                    delete movelist;
                    return false;
                }
            }
            movelist->clear();
        }
    }
    delete movelist;
    return true;
}

