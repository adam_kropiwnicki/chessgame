#include "numlineedit.h"

NumLineEdit::NumLineEdit(QWidget *parent)
    : QLineEdit(parent)
{

}

void NumLineEdit::sendValue()
{
    int x = QStringToInt(text());
    emit valueSent(x);
}
