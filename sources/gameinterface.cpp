#include "gameinterface.h"

void GameInterface::initBoard()
{
    if (Configuration::config().isValid() && _board == nullptr) {
        _board = new Board();
        connect(_board,SIGNAL(callEngine(Board*,Move)),_engine,SLOT(processMove(Board*,Move)));
        connect(_board,SIGNAL(gameOver(int)),this->parent(),SLOT(endGame(int)));
        connect(undoButton,SIGNAL(clicked(bool)),_board,SLOT(undoMove()));
        undoButton->show();
        if (server != nullptr) {
            connect(_board,SIGNAL(commitMove(Move)),server,SLOT(sendMove(Move)));
            connect(server,SIGNAL(requestMove(int,int,int,int,char)),_board,SLOT(moveRequested(int,int,int,int,char)));
        }
        if (client != nullptr) {
            connect(_board,SIGNAL(commitMove(Move)),client,SLOT(sendMove(Move)));
            connect(client,SIGNAL(requestMove(int,int,int,int,char)),_board,SLOT(moveRequested(int,int,int,int,char)));
        }
        if (minMax_ai == nullptr && (Configuration::config().blackController() == Configuration::AI || Configuration::config().whiteController() == Configuration::AI)) {
            minMax_ai = new AI(this,_engine);
            connect(minMax_ai,SIGNAL(requestMove(int,int,int,int,char)),_board,SLOT(moveRequested(int,int,int,int,char)));
            connect(_board,SIGNAL(callAI(Board*)),minMax_ai,SLOT(computeMove(Board*)));
        }
        scene->addItem(_board);
        if (minMax_ai != nullptr && Configuration::config().whiteController()==Configuration::AI) {
            minMax_ai->computeMove(_board);
        }
    }
    if (_board == nullptr) {
        QTimer::singleShot(2000, this,&GameInterface::initBoard);
    }
}

/* *********************************************************************************************
 * ****************************PUBLIC METHODS***************************************************
 * ********************************************************************************************/

GameInterface::GameInterface(QWidget* parent)
    : QFrame(parent),
      _board(nullptr),
      server(nullptr),
      client(nullptr),
      minMax_ai(nullptr)
{
    state = true;
    scene = new QGraphicsScene(this);           //Initialization
    view = new QGraphicsView(scene,this);
    _layout = new QGridLayout();
    w_text1 = new QLabel("White: ");
    b_text1 = new QLabel("Black: ");
    w_text2 = new QLabel("Gracz 1");
    b_text2 = new QLabel("Gracz 2");
    undoButton = new QPushButton("Undo move");
    undoButton->hide();

    if (Configuration::config().isValid()) {
        if (Configuration::config().whiteController() == Configuration::HUMAN || Configuration::config().blackController() == Configuration::HUMAN) {
            server = new Server(this);
            _layout->addWidget(server,6,8);
        }
    }
    else {
        client = new Client(this);
        connect(client,SIGNAL(goBack()),this,SLOT(abortAndRetreat()));
        client->run();
    }
    _layout->addWidget(view,0,0,7,7);
    _layout->addWidget(w_text1,0,8); //Sprawdz co to QLayoutItem
    _layout->addWidget(w_text2,0,9);
    _layout->addWidget(b_text1,1,8);
    _layout->addWidget(b_text2,1,9);
    _layout->addWidget(undoButton,5,8,1,1);

    setLayout(_layout);

    _engine = new Engine(this);
    initBoard();
}

GameInterface::~GameInterface()
{
    if (_board != nullptr)
        delete _board;
    if (_engine != nullptr)
        delete _engine;
    if (minMax_ai != nullptr)
        delete minMax_ai;
    if (server != nullptr)
        delete server;
    if (client != nullptr)
        delete client;
    delete _layout;
    delete view;
    delete scene;
}

void GameInterface::abortAndRetreat()
{
    emit abortingAndRetreating();
    state = false;
    return;
}
