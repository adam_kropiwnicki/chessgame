#include "mainwindow.h"
#include <iostream>
#include "configurtion.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      gameInterface(nullptr)
{
    connect(this,SIGNAL(resetConfiguration()),&(Configuration::config()),SLOT(reset()));
    menuInterface = new MenuInterface(this);
    connect(menuInterface,SIGNAL(gameSet()),this,SLOT(startGame()));
    menu();
}

MainWindow::~MainWindow()
{

}

bool MainWindow::startGame()
{
    gameInterface = new GameInterface(this);
    connect(gameInterface,SIGNAL(abortingAndRetreating()),this,SLOT(menu()));
    if (!gameInterface->isValid()) {
        menu();
        return 0;
    }
    setCentralWidget(gameInterface);
    menuInterface->hide();
    return 1;
}

void MainWindow::endGame(int side)
{
    if (side == 1) {
        winScreen = new QLabel("Black Wins!");
    }
    if (side == 0) {
        winScreen = new QLabel("White Wins!");
    }
    if (side == 2) {
        winScreen = new QLabel("Draw!");
    }
    gameInterface->hide();
    setCentralWidget(winScreen);
}

void MainWindow::menu()     //
{
    if (Configuration::config().isValid())
    {
        emit resetConfiguration();
        return;
    }
    menuInterface->showMainMenu();
    menuInterface->show();
    if (gameInterface != nullptr) {
        delete gameInterface;
        gameInterface = nullptr;
    }
    setCentralWidget(menuInterface);
}
