#include "configurtion.h"

Configuration::Configuration(QObject *parent)
    : QObject(parent)
{
    _rows = 0;
    _cols = 0;
    _tileSize = 0;
    _blacksColor = new QColor(Qt::GlobalColor::darkRed);
    _whitesColor = new QColor(Qt::GlobalColor::yellow);
    startingPoint = nullptr;
    _blackController = INVALID;
    _whiteController = INVALID;
}

Configuration::~Configuration()
{
    delete _blacksColor;
    delete _whitesColor;
    if(startingPoint != nullptr)
        delete startingPoint;
}

Configuration& Configuration::config()
{
    static Configuration con(Q_NULLPTR);
    return con;
}

bool Configuration::isValid() const
{
    if (_rows != 0 && _cols != 0 && _tileSize != 0 )
        return true;
    return false;
}
/*------------------------------------------------------------*
 * --------------------PRIVATE SLOTS--------------------------*
 * -----------------------------------------------------------*/

bool Configuration::set_cols(int x)
{
    _cols = x;
    return true;
}

bool Configuration::set_tileSize(int x)
{
    _tileSize = x;
    return true;
}

bool Configuration::set_rows(int x)
{
    _rows = x;
    return true;
}

bool Configuration::set_StandardGame()
{
    _rows = 8;
    _cols = 8;
    _tileSize = 120;

 //   _blackController = AI; // TESTING ONLY
 //   _whiteController = YOU;

    startingPoint = new Situation(_rows,_cols);
    for (int i =0;i<8;i++) {
        startingPoint->pieces[i][1] = 'p';
        startingPoint->pieces[i][6] = 'P';
    }
    startingPoint->pieces[0][0] = 'r';
    startingPoint->pieces[1][0] = 'k';
    startingPoint->pieces[2][0] = 'b';
    startingPoint->pieces[3][0] = 'q';
    startingPoint->pieces[4][0] = 'g';
    startingPoint->pieces[5][0] = 'b';
    startingPoint->pieces[6][0] = 'k';
    startingPoint->pieces[7][0] = 'r';
    startingPoint->pieces[0][7] = 'R';
    startingPoint->pieces[1][7] = 'K';
    startingPoint->pieces[2][7] = 'B';
    startingPoint->pieces[3][7] = 'Q';
    startingPoint->pieces[4][7] = 'G';
    startingPoint->pieces[5][7] = 'B';
    startingPoint->pieces[6][7] = 'K';
    startingPoint->pieces[7][7] = 'R';
    return true;
}

bool Configuration::set_StartingPoint(Situation s)
{
    if (startingPoint == nullptr)
         startingPoint = new Situation(s);
    if (startingPoint == nullptr)
        return false;
    return true;
}

bool Configuration::set_whiteController(int x)
{
    _whiteController = x;
    return true;
}

bool Configuration::set_blackController(int x)
{
    _blackController = x;
    return true;
}

void Configuration::reset()
{
    _rows = 0;
    _cols = 0;
    _tileSize = 0;
    if (startingPoint != nullptr) {
        delete startingPoint;
        startingPoint = nullptr;
    }

}

/*-------------------------------------------------------------*
 * ----------------------SITUATION SECTION---------------------*
 * ------------------------------------------------------------*/
Situation::Situation(int x,int y) {
    columns = x; rows = y; startingPlayer = false;
    pieces = new char*[columns];
    for (int i=0; i<columns; i++){
       pieces[i] = new char[rows];
       for (int j=0; j<rows; j++) {
           pieces[i][j] = '-';
       }
    }
}
Situation::Situation(const Situation& copied) {                 // REALLY BAD DESING, RETURNING AND PASSING BY VALUE RESULTING IN MULTIPLE
    columns = copied.columns; rows = copied.rows;               //ALLOCATIONS AND DEALLOCAIONS ---> TIME CONSUMING !!!!
    pieces = new char*[columns];
    for (int i=0; i<columns; i++){
        pieces[i] = new char[rows];
    }
    startingPlayer = copied.startingPlayer;
    for (int i=0;i<columns;i++) {
        for (int j=0;j<rows;j++)
            pieces[i][j] = copied.pieces[i][j];
    }
}
Situation::~Situation() {
    for (int i=columns-1;i>=0;i--){
        delete[] pieces[i];
    }
    delete[] pieces;
}
QString Situation::toQString() const {
    QString ret = "S";
    for (int i=0;i<columns;i++) {
        for (int j=0;j<rows;j++) {
            ret += pieces[i][j];
        }
    }
    ret += "E";
    return ret;
}
bool Situation::fromQString(QString from) {
    int k = 0;
    std::string tmp = from.toStdString();
    for (int i=0;i<columns;i++) {
        for (int j=0;j<rows;j++) {
            if (k<tmp.size()) {
                pieces[i][j] = tmp[k++];
            }
            else {
                return false;
            }
        }
    }
    return true;
}
