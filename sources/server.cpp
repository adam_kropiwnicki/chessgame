#include "server.h"
#include "utility.h"
#include <QMessageBox>
#include <iostream>
#include <QLabel>
#include <QVBoxLayout>
#include "configurtion.h"

Server::Server(QWidget *parent) : QWidget(parent),
    server(Q_NULLPTR)
{
    server = new QTcpServer(this);
    socket = nullptr;
    in.setVersion(QDataStream::Qt_4_0);
    if (!server->listen()) {
        QMessageBox::critical(this,"Host Server","Unable to start the server");
        return;
    }
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost && ipAddressesList.at(i).toIPv4Address()) {
            _ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (_ipAddress.isEmpty())
        _ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    _port = server->serverPort();

    QLabel *ipLabel = new QLabel("IP address:   "+_ipAddress,this);
    QLabel *portLabel = new QLabel("Port:   "+IntToQString(_port),this);
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(ipLabel);
    layout->addWidget(portLabel);

    connect(server,SIGNAL(newConnection()),this,SLOT(sendConfiguration()));
    //server->close();
    return;
}

Server::~Server()
{
    server->close();
}

void Server::sendConfiguration()
{
    QByteArray block;
    QDataStream out(&block,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << 'C'+IntToQString(Configuration::config().cols())+'X'+IntToQString(Configuration::config().rows())+'X'+IntToQString(Configuration::config().tile_size())+'X'+IntToQString(Configuration::config().whiteController())+'X'+IntToQString(Configuration::config().blackController())+'X'+Configuration::config().startPoint().toQString();
    socket = server->nextPendingConnection();
    if (socket != 0) {
        socket->write(block);
    }
    connect(socket,SIGNAL(readyRead()),this,SLOT(decryptMessage()));
    in.setDevice(socket);
    return;
}

void Server::sendMove(Move m)
{
    QByteArray block;
    QDataStream out(&block,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out <<'M'+IntToQString(m.from->column())+'X'+IntToQString(m.from->row())+'X'+IntToQString(m.target->column())+'X'+IntToQString(m.target->row())+'X'+*m.promotion+'X';
    if (socket != 0) {
        socket->write(block);
    }
    return;
}

void Server::decryptMessage()
{
    in.startTransaction();

    QString message;
    in >> message;

    if (!in.commitTransaction())
        return;

    if (message[0] != 'M') {
        //error report?
        return;
    }

    bool intDecoding = false;
    QString intTmp;
    char promotion;
    int values[4] = { -1, -1, -1, -1 };
    int j=0;

    for (int i=1;i<message.size();i++){
        if (message[i]=='X') {
            if (intDecoding) {
                intDecoding = false;
                values[j] = intTmp.toInt();
                j++;
                intTmp.clear();
            }
            continue;
        }
        if (message[i].isDigit()) {
            intDecoding = true;
            intTmp += message[i];
            continue;
        }
        else {
            promotion = message[i].toLatin1();
        }
    }
    for (j=0;j<4;j++) {
        if (values[j] == -1) {
            std::cout<<"Error in decrypting";
            //emit an error signal???
            return;
        }
    }
    emit requestMove(values[0],values[1],values[2],values[3],promotion);
    return;
}
