#include "move.h"

Move::Move(Piece *p, Tile *t)
{
    target = t;
    piece = p;
    from = p->tile();
    taken = t->piece();
    promotion = new char('-');
}

Move::Move(const Move & copied)
{
    target = copied.target;
    piece = copied.piece;
    from = copied.from;
    taken = copied.taken;
    promotion = new char(*copied.promotion);
}

Move &Move::operator=(Move copied)
{
    target = copied.target;
    piece = copied.piece;
    from = copied.from;
    taken = copied.taken;
    promotion = new char(*copied.promotion);
}

Move::~Move()
{
    delete promotion;
}
