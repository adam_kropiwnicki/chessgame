#include "pawn.h"
#include <QPainter>

/*---------------------------------------------------------------------------------------------------
 * ------------------------------------PUBLIC MEMBERS------------------------------------------------
 * ------------------------------------------------------------------------------------------------*/
Pawn::Pawn(QPointF p, bool c,QGraphicsObject *parent)
    : Piece(p,c,parent)
{
    if (c)
        _image = QPixmap(":/images/blackpawn.png");
    else
        _image = QPixmap(":/images/whitepawn.png");
    _boundingRect = QRectF(QPointF(-1* (_image.width() /2),-1 * (_image.height()/2)),QSize(_image.width(),_image.height()));
}

Pawn::~Pawn()
{

}

QRectF Pawn::boundingRect() const
{
    return _boundingRect;
}

void Pawn::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{
    painter->drawPixmap(boundingRect().topLeft(),_image);
}

bool Pawn::canMove(std::vector<Tile*> *moves,Tile *target) const
{
    if (isBlack()) {
        if (tile()->adjacent(1)!=nullptr) {
            if (tile()->adjacent(1)->piece()==nullptr) {
                    moves->push_back(tile()->adjacent(1));
                if (!hasMoved()) {
                    if (tile()->adjacent(1)->adjacent(1)!=nullptr) {
                        if (tile()->adjacent(1)->adjacent(1)->piece()==nullptr)
                            moves->push_back(tile()->adjacent(1)->adjacent(1));
                    }
                }
            }
        }
        if (tile()->adjacent(0)!=nullptr) {
            if (tile()->adjacent(0)->piece()!=nullptr && tile()->adjacent(0)->piece()->isBlack() != isBlack())                                                  //take check
                moves->push_back(tile()->adjacent(0));
            if (tile()->adjacent(0)->piece()==nullptr && tile()->adjacent(7)->piece() != nullptr &&  tile()->adjacent(7)->piece()->isBlack() != isBlack() && tile()->adjacent(7)->piece()->hasMoved() == 1) // flying take 1st check
                moves->push_back(tile()->adjacent(0));
        }
        if (tile()->adjacent(2)!=nullptr) {
            if (tile()->adjacent(2)->piece()!=nullptr && tile()->adjacent(2)->piece()->isBlack() != isBlack())
                moves->push_back(tile()->adjacent(2));
            if (tile()->adjacent(2)->piece()==nullptr && tile()->adjacent(3)->piece() != nullptr &&  tile()->adjacent(3)->piece()->isBlack() != isBlack() && tile()->adjacent(3)->piece()->hasMoved() == 1)
                moves->push_back(tile()->adjacent(2));
        }
    }
    else {
        if (tile()->adjacent(5)!=nullptr) {
            if (tile()->adjacent(5)->piece()==nullptr) {
                moves->push_back(tile()->adjacent(5));
                if (!hasMoved()) {
                    if (tile()->adjacent(5)->adjacent(5)!=nullptr) {
                        if (tile()->adjacent(5)->adjacent(5)->piece()==nullptr)
                            moves->push_back(tile()->adjacent(5)->adjacent(5));
                    }
                }
            }
        }
        if (tile()->adjacent(4)!=nullptr) {
            if (tile()->adjacent(4)->piece()!=nullptr && tile()->adjacent(4)->piece()->isBlack() != isBlack())
                moves->push_back(tile()->adjacent(4));
            if (tile()->adjacent(4)->piece()==nullptr && tile()->adjacent(3)->piece() != nullptr && tile()->adjacent(3)->piece()->isBlack() != isBlack() && tile()->adjacent(3)->piece()->hasMoved() == 1)
                moves->push_back(tile()->adjacent(4));
        }
        if (tile()->adjacent(6)!=nullptr) {
            if (tile()->adjacent(6)->piece()!=nullptr && tile()->adjacent(6)->piece()->isBlack() != isBlack())
                moves->push_back(tile()->adjacent(6));
            if (tile()->adjacent(6)->piece()==nullptr && tile()->adjacent(7)->piece() != nullptr &&  tile()->adjacent(7)->piece()->isBlack() != isBlack() && tile()->adjacent(7)->piece()->hasMoved() == 1)
                moves->push_back(tile()->adjacent(6));
        }
    }
    if (target!=nullptr) {
        for (unsigned int i=0;i<moves->size();i++) {
            if ((*moves)[i] == target)
                return true;
        }
    }
    return false;
}
