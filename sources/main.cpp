#include "mainwindow.h"
#include <QApplication>
#include "utility.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.showMaximized();
  //  w.show();

    return a.exec();
}

QString IntToQString(int i) {
    QString tmp;
    char const digits[] = "0123456789";
    if (i==0) {
        tmp.prepend(digits[0]);
    }
    while (i > 0) {
        tmp.prepend(digits[i%10]);
        i /= 10;
    }
    return tmp;
}

int QStringToInt(QString s) {
    char const digits[] = "0123456789";
    int final=0,x=1;
    bool flag=false;
    for (int i = 1;i<s.length();i++) {
        x *= 10;
    }
    for (int i = 0;i<s.length();i++){
        for (int j = 0;j<10;j++){
            if (digits[j]==s[i]) {
                final += j * x;
                x /= 10;
                flag = true;
                break;
            }
        }
        if (!flag) {

        }
        flag = false;
    }
    return final;
}
