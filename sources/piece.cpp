#include "piece.h"
#include "configurtion.h"

Piece::Piece(QPointF p, bool c,QGraphicsObject *parent)
    : QGraphicsObject(parent)
{
    setPos(p);
    _color = c;
    _tile = nullptr;
    has_moved = 0;
    setFlag(QGraphicsItem::ItemIsMovable,true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

Piece::~Piece()
{

}

void Piece::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (isBlack()) {
        if (Configuration::config().blackController() == Configuration::YOU) {
            QGraphicsItem::mousePressEvent(event);
        }
    }
    else {
        if (Configuration::config().whiteController() == Configuration::YOU) {
            QGraphicsItem::mousePressEvent(event);
        }
    }
}

void Piece::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (isBlack()) {
        if (Configuration::config().blackController() == Configuration::YOU) {
            QGraphicsItem::mouseMoveEvent(event);
        }
    }
    else {
        if (Configuration::config().whiteController() == Configuration::YOU) {
            QGraphicsItem::mouseMoveEvent(event);
        }
    }
   // QGraphicsItem::mouseMoveEvent(event);
}

void Piece::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (isBlack()) {
        if (Configuration::config().blackController() == Configuration::YOU) {
            QGraphicsItem::mouseReleaseEvent(event);
            emit moving(this,event->scenePos());
        }
    }
    else {
        if (Configuration::config().whiteController() == Configuration::YOU) {
            QGraphicsItem::mouseReleaseEvent(event);
            emit moving(this,event->scenePos());
        }
    }
  //  QGraphicsItem::mouseReleaseEvent(event);
  //  emit moving(this,event->scenePos());

}
