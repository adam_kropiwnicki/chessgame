#include "king.h"
#include <QPainter>

/*---------------------------------------------------------------------------------------------------
 * ------------------------------------PUBLIC MEMBERS------------------------------------------------
 * ------------------------------------------------------------------------------------------------*/
King::King(QPointF p, bool c,QGraphicsObject *parent)
    : Piece(p,c,parent)
{
    if (c)
        _image = QPixmap(":/images/blackking.png");
    else
        _image = QPixmap(":/images/whiteking.png");
    _boundingRect = QRectF(QPointF(-1* (_image.width() /2),-1 * (_image.height()/2)),QSize(_image.width(),_image.height()));
}

King::~King()
{

}

QRectF King::boundingRect() const
{
    return _boundingRect;
}

void King::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{
    painter->drawPixmap(boundingRect().topLeft(),_image);
}

bool King::canMove(std::vector<Tile*>* moves,Tile * target) const
{
    Tile * tmp = tile();
    for (int i=0; i<8; i++) {
        if (tmp->adjacent(i)!=nullptr) {
            if (tmp->adjacent(i)->piece()!=nullptr && tmp->adjacent(i)->piece()->isBlack() == isBlack()) {

            }
            else
                moves->push_back(tmp->adjacent(i));
        }
    }
    tmp = tile();
    if (!hasMoved()){
        while (tmp->adjacent(3)!=nullptr) {
            tmp = tmp->adjacent(3);
            if (tmp->piece()!= nullptr){
                if (tmp->piece()->type()==Piece::ROCK && !tmp->piece()->hasMoved()) {
//                   moves->push_back(tmp->adjacent(7));

                }
                else {
                    break;
                }
            }
        }
        tmp = tile();
        while (tmp->adjacent(7)!=nullptr) {
            tmp = tmp->adjacent(7);
            if (tmp->piece()!= nullptr){
                if (tmp->piece()->type()==Piece::ROCK && !tmp->piece()->hasMoved()) {
//                    moves->push_back(tmp->adjacent(3)->adjacent(3));
                }
                else {
                    break;
                }
            }
        }
    }
    if (target!=nullptr) {
        for (unsigned int i=0;i<moves->size();i++) {
            if ((*moves)[i] == target)
                return true;
        }
    }
    return false;
}
