#include "menuinterface.h"
#include "utility.h"


/* ----------------------------------------------
 * ---------------PUBLIC-------------------------
 * ---------------------------------------------*/

MenuInterface::MenuInterface(QWidget *parent)
    : QFrame(parent)
{
    setFrameStyle(34);
    setLineWidth(3);

    layout = new QVBoxLayout();                         //initiate members
    hostButton = new QPushButton("Host game");
    connectButton = new QPushButton("Connect to game");
    exitButton = new QPushButton("Exit game");
    startButton = new QPushButton("Start game");
    backButton = new QPushButton("Go back");
    whitecontrollerBox = new QComboBox(this);
    blackcontrollerBox = new QComboBox(this);

    game_settings = new QFrame(this);
    game_settings_sublayout = new QGridLayout();
    rows_edit = new NumLineEdit();
    cols_edit = new NumLineEdit();
    tileSize_edit = new NumLineEdit();
    rows_edit_label = new QLabel("Rows:\n(6 - 16)");
    cols_edit_label = new QLabel("Columns:\n(6 - 16)");
    tileSize_edit_label = new QLabel("Size of single tile: \n(40 - 200)");
    whitecontroller_label = new QLabel("White: ");
    blackcontroller_label = new QLabel("Black: ");

    minButtonSize = new QSize(100,40);
    maxButtonSize = new QSize(400,100);
    boardSizeEditValidator = new NumEditValidator(6,16,this);
    tileSizeEditValidator = new NumEditValidator(40,200,this);

    hostButton->setMinimumHeight(maxButtonSize->height());    //customalize members
    connectButton->setMinimumHeight(maxButtonSize->height());
    exitButton->setMinimumHeight(maxButtonSize->height());
    startButton->setMinimumHeight(maxButtonSize->height());
    backButton->setMinimumHeight(maxButtonSize->height());
    rows_edit->setValidator(boardSizeEditValidator);
    cols_edit->setValidator(boardSizeEditValidator);
    tileSize_edit->setValidator(tileSizeEditValidator);
    whitecontrollerBox->addItem("Hot seat player");
    whitecontrollerBox->addItem("Remote player");
    whitecontrollerBox->addItem("AI - minmax");
    blackcontrollerBox->addItem("Hot seat player");
    blackcontrollerBox->addItem("Remote player");
    blackcontrollerBox->addItem("AI - minmax");

    layout->addWidget(hostButton);
    layout->addWidget(connectButton);
    layout->addWidget(exitButton);
    layout->addWidget(game_settings);
    layout->addWidget(startButton);
    layout->addWidget(backButton);
    game_settings_sublayout->addWidget(whitecontroller_label,0,0);
    game_settings_sublayout->addWidget(blackcontroller_label,1,0);
    game_settings_sublayout->addWidget(whitecontrollerBox,0,1);
    game_settings_sublayout->addWidget(blackcontrollerBox,1,1);
    game_settings_sublayout->addWidget(rows_edit_label,2,0);
    game_settings_sublayout->addWidget(rows_edit,2,1);
    game_settings_sublayout->addWidget(cols_edit_label,3,0);
    game_settings_sublayout->addWidget(cols_edit,3,1);
    game_settings_sublayout->addWidget(tileSize_edit_label,4,0);
    game_settings_sublayout->addWidget(tileSize_edit,4,1);

    game_settings->setFrameStyle(34);
    game_settings->setLineWidth(3);
    layout->setContentsMargins(550,200,550,200);
    setLayout(layout);
    game_settings->setLayout(game_settings_sublayout);

    //qRegisterMetaType<Configuration>();

    connect(hostButton,SIGNAL(clicked(bool)),this,SLOT(showLobbyMenu()));       //connect buttons etc.
    connect(backButton,SIGNAL(clicked(bool)),this,SLOT(showMainMenu()));
    connect(exitButton,SIGNAL(clicked(bool)),parent,SLOT(close()));
    connect(startButton,SIGNAL(clicked(bool)),&(Configuration::config()),SLOT(set_StandardGame()));   // ALPHA ONLY!!
    connect(startButton,SIGNAL(clicked(bool)),this,SLOT(prepareStartUp()));
    connect(connectButton,SIGNAL(clicked(bool)),this,SLOT(prepareStartUp()));
    connect(rows_edit,SIGNAL(editingFinished()),rows_edit,SLOT(sendValue()));
    connect(rows_edit,SIGNAL(valueSent(int)),&(Configuration::config()),SLOT(set_rows(int)));
    connect(cols_edit,SIGNAL(editingFinished()),cols_edit,SLOT(sendValue()));
    connect(cols_edit,SIGNAL(valueSent(int)),&(Configuration::config()),SLOT(set_cols(int)));
    connect(tileSize_edit,SIGNAL(editingFinished()),tileSize_edit,SLOT(sendValue()));
    connect(tileSize_edit,SIGNAL(valueSent(int)),&(Configuration::config()),SLOT(set_tileSize(int)));
    connect(this,SIGNAL(setWhiteController(int)),&(Configuration::config()),SLOT(set_whiteController(int)));
    connect(this,SIGNAL(setBlackController(int)),&(Configuration::config()),SLOT(set_blackController(int)));


    showMainMenu();
}

MenuInterface::~MenuInterface()
{
    delete layout;
    delete startButton;
    delete connectButton;
    delete exitButton;
    delete hostButton;
    delete backButton;
    delete rows_edit;
    delete rows_edit_label;
    delete cols_edit;
    delete cols_edit_label;
    delete tileSize_edit;
    delete tileSize_edit_label;
    delete minButtonSize;
    delete maxButtonSize;
}

/* ----------------------------------------------
 * ---------------PRIVATE SLOTS------------------
 * ---------------------------------------------*/

bool MenuInterface::showMainMenu()
{
    hostButton->show();
    connectButton->show();
    exitButton->show();
    game_settings->hide();
    startButton->hide();
    backButton->hide();
    return 1;
}

bool MenuInterface::showLobbyMenu()
{
    hostButton->hide();
    connectButton->hide();
    exitButton->hide();
    game_settings->show();
    startButton->show();
    backButton->show();
    return 1;
}

bool MenuInterface::isValidated()
{ 
    return 1;
}

bool MenuInterface::prepareStartUp()
{
   /* if (Configuration::config().rows() == 0 || Configuration::config().cols() == 0 || Configuration::config().tile_size() == 0) {
        return false;
    } */
    switch (whitecontrollerBox->currentIndex()) {
    case 0:
        emit setWhiteController(Configuration::YOU);
        break;
    case 1:
        emit setWhiteController(Configuration::HUMAN);
        break;
    case 2:
        emit setWhiteController(Configuration::AI);
        break;
    default:
        std::cout<<"Not quite what was planned\n";
        break;
    }

    switch (blackcontrollerBox->currentIndex()) {
    case 0:
        emit setBlackController(Configuration::YOU);
        break;
    case 1:
        emit setBlackController(Configuration::HUMAN);
        break;
    case 2:
        emit setBlackController(Configuration::AI);
        break;
    default:
        std::cout<<"Not quite what was planned\n";
        break;
    }
    emit gameSet();
    return true;
}


