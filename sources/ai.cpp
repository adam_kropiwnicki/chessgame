#include "ai.h"
#include <iostream>
double AI::rate()
{
    double rating = 0;
    int sign = 0;
    int row_numb = 0;
    double pawn_worth = 1;
    double knight_worth = 3;
    double bishop_worth = 3;
    double rock_worth = 5;
    double queen_worth = 9;

    for (int i=0;i<Configuration::config().cols();i++) {
        for (int j=0;j<Configuration::config().rows();j++) {
            if (board->tiles[i][j]->piece() != nullptr) {

                if (board->tiles[i][j]->piece()->isBlack() == color) {
                    sign = 1;
                }
                else {
                    sign = -1;
                }
                if (color) {
                    row_numb = board->_rows - board->tiles[i][j]->row();
                }
                else {
                    row_numb = board->tiles[i][j]->row();
                }


                switch(board->tiles[i][j]->piece()->type()) {
                case Piece::PAWN:
                    rating += (( pawn_worth + ( 0.02 * row_numb * row_numb ) ) * sign);
                    break;
                case Piece::BISHOP:
                    rating += (bishop_worth*sign);
                    break;
                case Piece::KNIGHT:
                    rating += (knight_worth*sign);
                    break;
                case Piece::QUEEN:
                    rating += (queen_worth*sign);
                    break;
                case Piece::ROCK:
                    rating += (rock_worth*sign);
                    break;
                case Piece::KING:
                    break;
                default:
                    break;
                }
            }
        }
    }
    return rating;
}

double AI::MinMax(double alpha, double beta, int level)
{
    int debughistory =  board->history.size();

    double minMax = 9999;
    double rating;
    bool isMax;
    if (level == depth) {
        board->undoMove();
        if (debughistory - board->history.size() != 1 ) {                                    // DEBUG
            std::cout<<"byc moze roszada w sumie?\n";
        }
        //board->_turn = !board->_turn;
        return rate();
    }
    std::vector<Tile*> *movelist = new std::vector<Tile*>();
    if (board->_turn == color)
        isMax = true;
    else
        isMax = false;

    if (board->_turn) {
        for (unsigned int i=0;i<blackOrder.size();i++) {
            if (!blackOrder[i]->isVisible()) {
                continue;
            }
            blackOrder[i]->canMove(movelist);
            for (unsigned int j=0;j<movelist->size();j++) {
                if (!blackOrder[i]->isVisible()) {                                       //TESTING & DEBUGING - ASSERTION
                    std::cout<<"Attempt to move an already taken piece (in AI) \n";      //
                }                                                                        //
                board->moveRequested(blackOrder[i]->tile()->column(),blackOrder[i]->tile()->row(),(*movelist)[j]->column(),(*movelist)[j]->row(),'q');
                if (board->_ok) {
                    if (board->history.size() - debughistory != 1 ) {                   // DEBUG
                        std::cout<<"byc moze roszada w sumie?\n";
                    }
                    if (board->_endgame) {
                        if (color) {
                            rating = 8000;
                        }
                        else {
                            rating = -8000;
                        }
                        board->undoMove();
                        if (debughistory - board->history.size() != 0 ) {             // DEBUG
                            std::cout<<"byc moze roszada w sumie?\n";
                        }
                       // board->_turn = !board->_turn;
                    }
                    else {
                        rating = MinMax(alpha,beta,level+1);
                    }
                    if (color) {
                        if (minMax > 9000) {
                            minMax = rating;
                            if (level == 0) {
                                chosenPiece = blackOrder[i];
                                chosenTile = (*movelist)[j];
                            }
                        }
                        else {
                            if (minMax < rating) {                                  //JEEZ OGARNIJ ZNAK
                                minMax = rating;
                                if (level == 0) {
                                    chosenPiece = blackOrder[i];
                                    chosenTile = (*movelist)[j];
                                }
                            }
                        }
                        if (minMax > alpha) {
                            alpha = minMax;
                        }
                    }
                    else {
                        if (minMax > 9000) {
                            minMax = rating;
                        }
                        else {
                            if (minMax > rating) {                                  //JEEZ OGARNIJ ZNAK
                                minMax = rating;
                            }
                        }
                        if (minMax < beta) {
                            beta = minMax;
                        }
                    }
                    if (beta <= alpha) {
                        delete movelist;
                        board->undoMove();
                        if (debughistory - board->history.size() != 1 ) {                                //DEBUG
                            std::cout<<"byc moze roszada w sumie?\n";
                        }
                        //board->_turn = !board->_turn;
                        return minMax;
                    }
                }
                else {
                    continue;
                }
            }
            movelist->clear();
        }
    }
    else {
        for (unsigned int i=0;i<whiteOrder.size();i++) {
            if (!whiteOrder[i]->isVisible()) {
                continue;
            }
            whiteOrder[i]->canMove(movelist);
            for (unsigned int j=0;j<movelist->size();j++) {
                if (!whiteOrder[i]->isVisible()) {                                       //TESTING & DEBUGING - ASSERTION
                    std::cout<<"Attempt to move an already taken piece (in AI) \n";      //
                }                                                                        //
                board->moveRequested(whiteOrder[i]->tile()->column(),whiteOrder[i]->tile()->row(),(*movelist)[j]->column(),(*movelist)[j]->row(),'q');
                if (board->_ok) {
                    if (board->history.size() - debughistory != 1 ) {                   // DEBUG
                        std::cout<<"byc moze roszada w sumie?\n";
                    }
                    if (board->_endgame) {
                        if (!color) {
                            rating = 8000;
                        }
                        else {
                            rating = -8000;
                        }
                        board->undoMove();
                        if (debughistory - board->history.size() != 0 ) {                                //DEBUG
                            std::cout<<"byc moze roszada w sumie?\n";
                        }
                        //board->_turn = !board->_turn;
                    }
                    else {
                        rating = MinMax(alpha,beta,level+1);
                    }
                    if (!color) {
                        if (minMax > 9000) {
                            minMax = rating;
                            if (level == 0) {
                                chosenPiece = whiteOrder[i];
                                chosenTile = (*movelist)[j];
                            }
                        }
                        else {
                            if (minMax < rating) {                                          //JEEZ OGARNIJ ZNAK
                                minMax = rating;
                                if (level == 0) {
                                    chosenPiece = whiteOrder[i];
                                    chosenTile = (*movelist)[j];
                                }
                            }
                        }
                        if (minMax > alpha) {
                            alpha = minMax;
                        }
                    }
                    else {
                        if (minMax > 9000) {
                            minMax = rating;
                         }
                         else {
                            if (minMax > rating) {                                          //JEEZ OGARNIJ ZNAK
                                minMax = rating;
                            }
                         }
                         if (minMax < beta) {
                            beta = minMax;
                         }
                    }
                    if (beta <= alpha) {
                         delete movelist;
                         board->undoMove();
                         if (debughistory - board->history.size() != 1 ) {                                //DEBUG
                             std::cout<<"byc moze roszada w sumie?\n";
                         }
                         //board->_turn = !board->_turn;
                         return minMax;
                    }
                }
                else {
                   continue;
                }
            }
            movelist->clear();
        }
    }
    delete movelist;
    if (level != 0) {
        std::vector<Move> debugkurwo = board->history;
        board->undoMove();
        if (debughistory - board->history.size() != 1 ) {                                //DEBUG
            std::cout<<"byc moze roszada w sumie?\n";
        }
       // board->_turn = !board->_turn;
    }
    return minMax;
}

AI::AI(QObject * parent,Engine *eng)
    : QObject (parent)
{
    board = new Board();
    color = false;
    depth = 4;
    connect(board,SIGNAL(callEngine(Board*,Move)),eng,SLOT(processMove(Board*,Move)));
}

AI::~AI()
{
    delete board;
}

void AI::computeMove(Board * brd)
{
    board->restoreBoard(brd);
    color = brd->_turn;
    chosenPiece = nullptr;
    chosenTile = nullptr;
    whiteOrder = board->whites;
    blackOrder = board->blacks;
    debug_historystartState = brd->history.size();
    MinMax(-10000,10000,0);
    if (chosenPiece == nullptr || chosenTile == nullptr) {
        std::cout<<"AI is stupid and worthless\n";
        return;
    }
    if (!board->history.empty()) {
        std::cout<<"Error in history processing in fake board\n";
    }
    emit requestMove(chosenPiece->tile()->column(),chosenPiece->tile()->row(),chosenTile->column(),chosenTile->row(),'q');
    return;
}
