#-------------------------------------------------
#
# Project created by QtCreator 2018-07-26T17:42:08
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyChess
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += "includes/"

SOURCES += \
    sources/ai.cpp \
    sources/bishop.cpp \
    sources/board.cpp \
    sources/client.cpp \
    sources/configuration.cpp \
    sources/engine.cpp \
    sources/gameinterface.cpp \
    sources/human.cpp \
    sources/king.cpp \
    sources/knight.cpp \
    sources/main.cpp \
    sources/mainwindow.cpp \
    sources/menuinterface.cpp \
    sources/move.cpp \
    sources/numeditvalidator.cpp \
    sources/numlineedit.cpp \
    sources/pawn.cpp \
    sources/piece.cpp \
    sources/player.cpp \
    sources/queen.cpp \
    sources/rock.cpp \
    sources/server.cpp \
    sources/tile.cpp

HEADERS += \
    includes/ai.h \
    includes/bishop.h \
    includes/board.h \
    includes/client.h \
    includes/configurtion.h \
    includes/engine.h \
    includes/gameinterface.h \
    includes/human.h \
    includes/king.h \
    includes/knight.h \
    includes/mainwindow.h \
    includes/menuinterface.h \
    includes/move.h \
    includes/numeditvalidator.h \
    includes/numlineedit.h \
    includes/pawn.h \
    includes/piece.h \
    includes/player.h \
    includes/queen.h \
    includes/rock.h \
    includes/server.h \
    includes/tile.h \
    includes/utility.h

RESOURCES += \
    assets.qrc
