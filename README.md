## This is an application for playing chess. It is still in "work in progress" state, as many of feature are not avaiable or simply not work properly. 
***
##### Implemented features:
- Playing standard 8x8 chess game on 'hot-seat'
- Playing standard 8x8 chess game online (Host IP and port has to be sent to the other player 'externally')
##### To-be-implemented features:
- Playing with AI (currently it is only really stupid MinMax algorithm
- Non-standard games with custom board and rules
***
Graphics would be updated only if I got really bored.
